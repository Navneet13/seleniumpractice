import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


//Tests the Input Box page on SeleniumEasy.com
//URL: https://www.seleniumeasy.com/test/basic-first-form-demo.html
public class TestInputBoxes {
	
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
	    //1. Setup Selenium + your Webdriver
		
			System.setProperty("webdriver.chrome.driver","/Users/navpreetkaur/Desktop/chromedriver");
			
			driver = new ChromeDriver();
			
	    //2. Go to website
			
			driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
	}

	@After
	public void tearDown() throws Exception {
		//4. Close the browser
		Thread.sleep(2000);
		driver.close();
	}

	
	//R1: Test Single Input Fields
	@Test
	public void testSingleInputField() {
		
		//3. Write code to do some stuff on that website
				
				//3a. Type something into the single input box
		
				// get the box (The box has a unique id)
		          WebElement inputBox = driver.findElement(By.id("user-message"));
				
				// Now type something in box
				
				   inputBox.sendKeys("Here is something new");
				
				//3b. Automatically put the submit button
				    // - Get the button first
				 WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
				
				
				
         //2. Push the button
				showMessageButton.click();
				
			
		//3. Get the actual output from the screen
				WebElement outputBox = driver.findElement(By.id("display"));
				String actualOutput = outputBox.getText();
				
		//4. Check if the expected output == actual output
				assertEquals("Here is something new",actualOutput);
		
		
	}
	
	//R2: Test Two Input Fields
	@Test
	public void testTwoInputFields() {

		//3. Writing the code
		WebElement inputBox1 = driver.findElement(By.id("sum1"));
		inputBox1.sendKeys("25");
		
		WebElement inputBox2 = driver.findElement(By.id("sum2"));
		inputBox2.sendKeys("50");
		
		
		//4. Push the button
		    // - Get the button
		WebElement button= driver.findElement(By.cssSelector("form#gettotal button"));
		button.click();
	
		//5. Get  the  outputBox
		WebElement outputBox = driver.findElement(By.id("displayvalue"));
		String actualOutput = outputBox.getText();
		
		//6. Check if actual = expected
		assertEquals("75",actualOutput);
	}

}
