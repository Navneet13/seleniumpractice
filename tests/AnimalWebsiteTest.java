import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AnimalWebsiteTest {
	
	//Location of chromedriver file
    final String CHROMEDRIVER_LOCATION = "/Users/navpreetkaur/Desktop/chromedriver";
   
    //Website we wwant to test
    final String URL_TO_TEST = "https://www.webdirectory.com/Animals/";
    
    //Global Driver declaration
	WebDriver driver;		

	@Before
	public void setUp() throws Exception {
		//Setuo Selenium

		System.setProperty("webdriver.chrome.driver",CHROMEDRIVER_LOCATION);
		
		driver = new ChromeDriver();
		
		//Go to website
		driver.get(URL_TO_TEST);
		
	}

	@After
	public void tearDown() throws Exception {
		//Close the browser
		// Close the browser
		Thread.sleep(2000);
		driver.close();
	}

	@Test
	public void testNumberOfLinks() {
	// Just checking if number of links is equal to 10
         //  1. Get all the bulleted llinks in section
		    List<WebElement> bulletedLinks = driver.findElements(By.cssSelector("table+ul li a"));
		    
	    
//		    //Solution 2 :
//		    List<WebElement> uls = driver.findElements(By.cssSelector("ul"));
//		    
//		    //get the first URL <ul>
//		    WebElement firstUL = uls.get(0);
//		    
//		    //get all links inside the first <ul>
//		    List<WebElement>bulletLinks = firstUL.findElements(By.cssSelector("li a"));
		    	
		    
		    //print on screen
		     System.out.println("Number of links on page: "+bulletedLinks.size());
		    
		 //  2. Output the links to the screen using System.out.println()
		     //Iterating through the list of links
		    for(int i=0; i < bulletedLinks.size(); i++)
		    {
		    	//Get the current link
		    	  WebElement link = bulletedLinks.get(i);
		    	  
		    	//get the link Text
		    	  String linkText = link.getText();
		    	  
		    	 //Get link url
		    	  String linkURL = link.getAttribute("href");
		    	  
		          //OR   String linkUrl = link.getAttribute("class");    
		    	  
		    	  
		    	 //Output to screen
		    	  System.out.println(linkText + ": " +linkURL);
		    	  
		    } 
		
		
	}

}
