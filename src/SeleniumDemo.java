import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDemo {
	
	public static void main(String[] args) throws InterruptedException {
		//1. Setup Selenium + your Webdriver
		
		System.setProperty("webdriver.chrome.driver","/Users/navpreetkaur/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		
		
		//2. Go to website
		
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		
		//3. Write code to do some stuff on that website
		
		//3a. Type something into the single input box
		// get the box (The box has a unique id)
		WebElement inputBox = driver.findElement(By.id("user-message"));
		
		// Type something in box
		
		inputBox.sendKeys("Here is something new");
		
		//3b. Automatically put the submit button
		 // - Get the button
		WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input"));
		
		 // -Push the button
		showMessageButton.click();
		
		//4. Close the browser
		Thread.sleep(2000);
		driver.close();
	}

}
